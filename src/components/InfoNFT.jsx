import React, { useState, useEffect } from "react";
import { useMoralisDapp } from "providers/MoralisDappProvider/MoralisDappProvider";
import axios from 'axios';
import { useIPFS } from "hooks/useIPFS";
import { Web3Context } from "../upload/Web3Provider";
import {
    useMoralis,
    useMoralisQuery,
} from "react-moralis";
import { Modal, Alert, Spin } from "antd";
import { useContext } from 'react'
import { useWeb3ExecuteFunction } from "react-moralis";
import { useHistory } from "react-router";
import { PolygonLogo } from "./Chains/Logos";
import Attributes from "./Attributes";
import NewAttributes from "./NewAttributes";
const SERVER_URL = process.env.REACT_APP_MORALIS_SERVER_URL;

function InfoNFT() {
    const queryParams = new URLSearchParams(window.location.search);
    const { nftContract } = useContext(Web3Context)
    const { chainId } = useMoralisDapp();
    const id = queryParams.get('id');
    const adrr = queryParams.get('adress');
    const price = queryParams.get('price');
    const [meta, setMeta] = useState(null)

    const [newAttribute, setNewAttribute] = useState(null)
    console.log("newAttribute", newAttribute)
    const [datas, setData] = useState(null)
    const { resolveLink } = useIPFS();
    const [pricer, setPrice] = useState(0);
    const getData = () => {
        axios.post(SERVER_URL + '/functions/getTokenIdMetadata/', {
            "address": adrr,
            "token_id": id,
            "chain": chainId
        })
            .then(res => {
                if (res.status === 200) {
                    setData(res?.data?.result)
                }
            })
    }
    useEffect(() => {
        getData()
    }, [chainId, id, adrr])

    useEffect(async () => {
        const NFT = datas;
        if (NFT?.metadata) {
            NFT.metadata = JSON.parse(NFT.metadata);
            NFT.image = resolveLink(NFT.metadata?.image);
        } else if (NFT?.token_uri) {
            try {
                await fetch(NFT.token_uri)
                    .then((response) => response.json())
                    .then((data) => {
                        NFT.image = resolveLink(data.image);
                    });
            } catch (error) {
            }
        }
        setMeta(NFT);
        setNewAttribute(NFT?.metadata)

    }, [datas]);

    const [visible, setVisibility] = useState(false);
    const [nftToBuy, setNftToBuy] = useState(null);
    const [loading, setLoading] = useState(false);
    const [status, setstatus] = useState(true);
    const [loadingUp, setLoadingUp] = useState(false);
    const contractProcessor = useWeb3ExecuteFunction();
    const { marketAddress, contractABI, walletAddress } =
        useMoralisDapp();
    const contractABIJson = JSON.parse(contractABI);
    const { Moralis } = useMoralis();
    const queryMarketItems = useMoralisQuery("MarketItems");
    const fetchMarketItems = JSON.parse(
        JSON.stringify(queryMarketItems.data, [
            "objectId",
            "createdAt",
            "price",
            "nftContract",
            "itemId",
            "sold",
            "tokenId",
            "seller",
            "owner",
            "confirmed",
        ])
    );
    console.log("walletAddress", walletAddress)
    console.log("data===", meta?.owner_of)
    const purchaseItemFunction = "createMarketSale";
    async function purchase() {
        setLoading(true);
        const tokenDetails = getMarketItem(meta);
        const itemID = tokenDetails.itemId;
        const tokenPrice = tokenDetails.price;
        const ops = {
            contractAddress: marketAddress,
            functionName: purchaseItemFunction,
            abi: contractABIJson,
            params: {
                nftContract: meta.token_address,
                itemId: itemID,
            },
            msgValue: tokenPrice,
        };
        await contractProcessor.fetch({
            params: ops,
            onSuccess: () => {
                console.log("success");
                setLoading(false);
                setVisibility(false);
                updateSoldMarketItem();
                succPurchase();
            },
            onError: (error) => {
                setLoading(false);
                failPurchase();
            },
        });
    }
    function succPurchase() {
        let secondsToGo = 5;
        const modal = Modal.success({
            title: "Success!",
            content: `You have purchased this NFT`,
        });
        setTimeout(() => {
            modal.destroy();
            history.push('/nftBalance');
        }, secondsToGo * 1000);
    }
    const savedDataAttributes = (item) => {
        // setNewExprense([expenses1,...newExpense])
        console.log('new item: ', item)
        meta.metadata.attributes = [...meta.metadata.attributes, item]
        setNewAttribute(meta.metadata.attributes);
    }
    const updateStatus = (item) => {
        setstatus(item);
    }
    // useEffect(() => {
    //     // console.log(props.metadata)
    //     setstatus(status);
    //   }, [status]);
    async function createIPFSNFT() {

        try {
            setLoadingUp(true);
            setShowModalMeta(false);
            const fileBlob = new Blob([JSON.stringify(meta.metadata)], {
                type: 'application/json',
            })
            var formData1 = new FormData()
            formData1.append('file', fileBlob, `${meta.metadata.name}.json`)
            formData1.append('duration', 525)
            formData1.append('file_type', 1)
            formData1.append('wallet_address', walletAddress)
            const { data } = await axios.post('https://calibration-mcs-api.filswan.com/api/v1/storage/ipfs/upload', formData1, {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            console.log(data)
            const transaction = await nftContract.setURI(meta.token_id, data.data.ipfs_url)
            const tx = await transaction.wait()
            setLoadingUp(false)
            updateMetadata()
        } catch (error) {
            setLoadingUp(false)
            failUpdateMetadata()
        } finally {
            setLoadingUp(false)
        }
    }
    function failUpdateMetadata() {
        let secondsToGo = 5;
        const modal = Modal.error({
            title: "Error!",
            content: `Update MetaData Failed`,
        });
        setTimeout(() => {
            modal.destroy();
        }, secondsToGo * 1000);
    }
    function updateMetadata() {
        let secondsToGo = 5;
        const modal = Modal.success({
            title: "Success!",
            content: `Update MetaData Success`,
        });
        setTimeout(() => {
            modal.destroy();
        }, secondsToGo * 1000);
    }
    const deleteData = (index) => {

        console.log(index)

        const temp = meta.metadata.attributes.splice(index, 1);
        setNewAttribute(temp);

    }
    function failPurchase() {
        let secondsToGo = 5;
        const modal = Modal.error({
            title: "Error!",
            content: `There was a problem when purchasing this NFT`,
        });
        setTimeout(() => {
            modal.destroy();
        }, secondsToGo * 1000);
    }
    async function updateSoldMarketItem() {
        const id = getMarketItem(meta).objectId;
        const marketList = Moralis.Object.extend("MarketItems");
        const query = new Moralis.Query(marketList);
        await query.get(id).then((obj) => {
            obj.set("sold", true);
            obj.set("owner", walletAddress);
            obj.save();
        });
    }

    const getMarketItem = (nft) => {
        const result = fetchMarketItems?.find(
            (e) =>
                e.nftContract === nft?.token_address &&
                e.tokenId === nft?.token_id &&
                e.sold === false &&
                e.confirmed === true

        );
        return result;
    };

    const listItemFunction = "createMarketItem";
    const ItemImage = Moralis.Object.extend("ItemImages");
    async function list(nft, listPrice) {
        setLoading(true);
        const p = listPrice * ("1e" + 18);
        const ops = {
            contractAddress: marketAddress,
            functionName: listItemFunction,
            abi: contractABIJson,
            params: {
                nftContract: nft.token_address,
                tokenId: nft.token_id,
                price: String(p),
            },
        };

        await contractProcessor.fetch({
            params: ops,
            onSuccess: () => {
                setLoading(false);
                setVisibility(false);
                addItemImage();
                succList();
            },
            onError: (error) => {
                setLoading(false);
                failList();
            },
        });
    }
    const history = useHistory();
    async function approveAll(nft) {
        setLoading(true);
        const ops = {
            contractAddress: nft.token_address,
            functionName: "setApprovalForAll",
            abi: [{ "inputs": [{ "internalType": "address", "name": "operator", "type": "address" }, { "internalType": "bool", "name": "approved", "type": "bool" }], "name": "setApprovalForAll", "outputs": [], "stateMutability": "nonpayable", "type": "function" }],
            params: {
                operator: marketAddress,
                approved: true
            },
        };
        await contractProcessor.fetch({
            params: ops,
            onSuccess: () => {
                list(meta, pricer);
            },
            onError: (error) => {
                setLoading(false);
                failApprove();
            },
        });
    }
    function succList() {
        let secondsToGo = 5;
        const modal = Modal.success({
            title: "Success!",
            content: `Your NFT was listed on the marketplace`,
        });
        setTimeout(() => {
            modal.destroy();
            history.push('/MyNFT')
        }, secondsToGo * 1000);
    }
    function failList() {
        let secondsToGo = 5;
        const modal = Modal.error({
            title: "Error!",
            content: `There was a problem listing your NFT`,
        });
        setTimeout(() => {
            modal.destroy();
        }, secondsToGo * 1000);
    }
    function failApprove() {
        let secondsToGo = 5;
        const modal = Modal.error({
            title: "Error!",
            content: `There was a problem with setting approval`,
        });
        setTimeout(() => {
            modal.destroy();
        }, secondsToGo * 1000);
    }
    function addItemImage() {
        const itemImage = new ItemImage();
        itemImage.set("image", meta.image);
        itemImage.set("nftContract", meta.token_address);
        itemImage.set("tokenId", meta.token_id);
        itemImage.set("name", meta.name);
        itemImage.save();
    }
    const [show, setShow] = useState(false)
    const [showModalMeta, setShowModalMeta] = useState(false)
    const copyToClipboardWithCommand = (content) => {
        const el = document.createElement("textarea");
        el.value = content;
        document.body.appendChild(el);
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
    };
    const [isClicked, setIsClicked] = useState(false);
    const [isClickedowner, setIsClickedowner] = useState(false);
    const Copy = ({data}) => (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="26"
            height="26"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="#1780FF"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            style={{ cursor: "pointer",verticalAlign: 'top' }}
            onClick={() => {
                navigator.clipboard.writeText(data);
                setIsClicked(true);
            }}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M15 3v4a1 1 0 0 0 1 1h4" />
            <path d="M18 17h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h4l5 5v7a2 2 0 0 1 -2 2z" />
            <path d="M16 17v2a2 2 0 0 1 -2 2h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h2" />
            <title id="copy-address">Copy Address</title>
        </svg>
    );
    const Check = () => (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="3"
            stroke="#21BF96"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M5 12l5 5l10 -10" />
            <title id="copied-address">Copied!</title>
        </svg>
    );
    const Copyowner = ({data}) => (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="26"
            height="26"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="#1780FF"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            style={{ cursor: "pointer",verticalAlign: 'top' }}
            onClick={() => {
                navigator.clipboard.writeText(data);
                setIsClickedowner(true);
            }}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M15 3v4a1 1 0 0 0 1 1h4" />
            <path d="M18 17h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h4l5 5v7a2 2 0 0 1 -2 2z" />
            <path d="M16 17v2a2 2 0 0 1 -2 2h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h2" />
            <title id="copy-address">Copy Address</title>
        </svg>
    );
    const CheckOwner = () => (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="3"
            stroke="#21BF96"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M5 12l5 5l10 -10" />
            <title id="copied-address">Copied!</title>
        </svg>
    );
    return (
        <Spin spinning={loadingUp} >
            <div style={{ display: 'block', width: '100%' }}>
                <h4 className='titleInfo slideNFT'>{!price ? 'Sell NFT 🖼 ' : 'Buy NFT 🛒'}</h4>
                <div className='dflex infoNFt slideNFT'>
                    <div >
                        <div className="border-ras" >
                            <Spin spinning={loading} >

                                {
                                    meta?.metadata?.name?.split('.')[1] === 'mp4' || meta?.metadata?.name?.split('.')[1] === 'webm' ? (
                                        <video src={meta?.image} style={{ height: '265px', width: '100%', margin: 'auto' }} autoPlay loop muted onClick={() => setShow(true)}></video>
                                    ) : (
                                        <img src={meta?.image} alt="" style={{ margin: 'auto', height: '265px' }} onClick={() => setShow(true)} />
                                    )
                                }

                            </Spin>

                        </div>

                        <div className="border-des mt-5">
                            <p className="box" style={{ textAlign: 'center', color: '#47a2f7', fontSize: '25px' }}>{meta?.metadata?.name}</p>
                            <div className="dflex">
                                <label className="boxx">Price:</label>
                                {
                                    !price ? (<input type='text' className="inputstyle" placeholder="Price in Matic ..." onChange={(event) => setPrice(event.target.value)} />) : (<span className="spanstyle" >{price} MATIC</span>)
                                }
                                <PolygonLogo />
                            </div>
                        </div>
                    </div>
                    <div className="border-des ml">
                        <label className="box" style={{ borderBottom: 'none' }}>Description: </label>
                        <p className="txt" style={{ width: '90%', margin: 'auto' }}>{meta?.metadata?.description}</p>
                        <hr />
                        <span className="box">About:  <p style={{ paddingLeft: '10px', color: '#47a2f7' }}>{meta?.name}</p></span>
                        <label className="box" style={{ borderBottom: 'none' }}>Details</label>
                        <div className="details">
                            <div style={{ display: 'grid', width: '40%' }}>
                                <span className="txt">Constract Address:</span>
                                <span className="txt">Owner:</span>
                                <span className="txt">TokenID:</span>
                                <span className="txt">Token Standand: </span>
                                <span className="txt">Chain: </span>
                            </div>
                            <div style={{ display: 'grid', width: '60%' }}>
                                <span className="txt">{meta?.token_address.slice(0, 5)}...{meta?.token_address.slice(30, 42)}{isClicked ? <Check /> : <Copy data={meta?.token_address} />}</span>
                                <span className="txt" title={meta?.owner_of}>{meta?.owner_of.slice(0, 5)}...{meta?.owner_of.slice(30, 42)}{isClickedowner ? <CheckOwner /> : <Copyowner data={meta?.owner_of} />}</span>
                                <span className="txt">{meta?.token_id}</span>
                                <span className="txt">{meta?.contract_type}</span>
                                <span className="txt">{chainId}</span>
                            </div>
                        </div>
                    </div>
                </div >
                <div className="border-des slideNFT" style={{ marginTop: '20px', marginBottom: '20px' }}>
                    <div>
                        <label className="box" style={{ borderBottom: 'none' }}>Attributes</label>
                    </div>

                    <div className="details">

                        <div style={{ display: 'grid', width: '40%' }}>
                            <h3>Trait type</h3>
                            {
                                meta?.metadata?.attributes.map((item) => (
                                    <>
                                        <span className="txt">{item.trait_type}</span>
                                    </>

                                ))
                            }
                        </div>
                        <div style={{ display: 'grid', width: '60%' }}>
                            <h3>Value</h3>
                            {
                                meta?.metadata?.attributes.map((item) => (
                                    <>
                                        <span className="txt">{item.value}</span>
                                    </>
                                ))
                            }
                        </div>
                    </div>
                </div>
                {
                    price === '0' ? (
                        <Alert className="slideNFT" style={{ textAlign: 'center', marginBottom: "30px" }}
                            message="This NFT is currently not for sale"
                            type="warning"
                        />
                    ) : []
                }
                <div className="slideNFT" style={{ marginTop: '20px', display: "flex" }} >
                    {

                        price ? (
                            <>
                                <Spin spinning={loading}>
                                    <button className="buy-btn" onClick={() => purchase()}>Buy</button>
                                </Spin >
                            </>
                        ) : (
                            <>
                                <Spin spinning={loading}>
                                    <button className="buy-btn" disabled={pricer === 0} onClick={() => approveAll(meta)}>Sell</button>
                                </Spin >
                            </>
                        )

                    }
                    <div style={{ marginLeft: "10px", backgroundColor: "#1d58e2", marginBottom: "30px", display: meta?.owner_of === walletAddress ? "block" : "none" }}>
                        <button className="buy-btn " onClick={() => setShowModalMeta(true)}>Update MetaData</button>
                    </div >
                </div>

                {show ? (
                    <Modal
                        title={`${meta?.name} #${meta?.token_id}`}
                        visible={show}
                        onCancel={() => setShow(false)}
                        onOk={() => setShow(false)}>
                        <div
                            style={{
                                width: "70%",
                                margin: "auto",
                            }}>
                            <>
                                {
                                    meta?.metadata?.name?.split('.')[1] === 'mp4' || meta?.metadata?.name?.split('.')[1] === 'webm' ? (
                                        <video src={meta?.metadata?.image} controls muted style={{ borderRadius: '10px', padding: '5px', width: "100%", margin: 'auto', }}></video>
                                    ) : (
                                        <img
                                            src={meta?.image}
                                            style={{
                                                width: "80%",
                                                margin: 'auto',
                                                borderRadius: "10px",
                                                marginBottom: "15px",
                                            }} />
                                    )
                                }
                            </>
                        </div>
                    </Modal>
                ) : []}
                {showModalMeta ? (
                    <Modal
                        title={`Attributes #${meta?.token_id}`}
                        visible={showModalMeta}
                        onCancel={() => setShowModalMeta(false)}
                        onOk={() => createIPFSNFT()}
                        okButtonProps={{ disabled: !status }} >
                        <div
                            style={{
                                width: "90%",
                                margin: "auto",
                            }}>

                            {
                                <>
                                    <NewAttributes onsavedata={savedDataAttributes} metadata={meta?.metadata} status2={updateStatus} />
                                    <Attributes metadata={meta?.metadata} onDelData={deleteData} status={updateStatus} />
                                    {/* <span>
                  {JSON.stringify(meta?.metadata.attributes)}
                </span> */}
                                    {/* <Attributes/> */}
                                </>
                            }

                        </div>
                    </Modal>
                ) : []}
            </div>
        </Spin>
    );
}

export default InfoNFT;
