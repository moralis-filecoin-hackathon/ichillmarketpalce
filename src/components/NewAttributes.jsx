import { React, useState, useEffect } from "react";
export default function NewAttributes(props) {
  const [enterKey, setEnterKey] = useState("");
  const [status, setStatus] = useState(false);
  const [enterValue, setEnterValue] = useState("");

  useEffect(() => {
    // console.log(props.metadata)
    props.status2(status);
  }, [status]);

  const titlechange = (event) => {
    const result = validator(event.target.value);
    if (result) {
      setStatus(true);
    } else {
    
        setStatus(false);
      
    }
    setEnterKey(event.target.value);
    console.log("test", event.target.value);
    // props.status2(status);
  };
  const titlechangeValue = (event) => {
    setEnterValue(event.target.value);
    console.log("test", event.target.value);
  };

  const submitForm = (event) => {
    event.preventDefault();
    const attr = {
      trait_type: enterKey,
      value: enterValue,
    };
    props.onsavedata(attr);
    console.log("data", attr);
    setEnterKey("");
    setEnterValue("");
  };
  const validator = (data) => {
    let flag = false;
    // get value input
    const re = /^[0-9a-zA-Z_]+$/;
    const isFound = props.metadata.attributes.some((element) => {
      if (element.trait_type === data) {
        return true;
      }

      return false;
    });
    // console.log("testdata",data.includes(' ') || !re.test(data))
    if (data.includes(" ") || !re.test(data) || data.length == 0 || isFound) {
      flag = false;
    } else {
      flag = true;
    }

    return flag;
  };
  return (
    <div>
      {
        <>
          <form onSubmit={submitForm}>
            <div style={{ display: "flex" }}>
              <div style={{ display: "grid", width: "30%" }}>
                <input
                  style={{
                    borderColor: "rgb(52, 131, 235)",
                    borderStyle: "solid",
                  }}
                  className={`FormInput_input_popup ${
                    !status ? "invalid" : ""
                  }`}
                  value={enterKey}
                  placeholder="Enter a key"
                  onChange={titlechange}
                ></input>
              </div>
              <div style={{ display: "grid", width: "65%" }}>
                <input
                  style={{
                    borderColor: "rgb(52, 131, 235)",
                    borderStyle: "solid",
                  }}
                  className="FormInput_input_popup "
                  value={enterValue}
                  placeholder="Enter a value"
                  onChange={titlechangeValue}
                ></input>
              </div>
              <div style={{ display: "grid", width: "5%" }}>
                <div className="FormInput_delete_popup">
                  <button
                    style={{ borderRadius: 10 }}
                    disabled={!status}
                    type="submit"
                  >
                    {" "}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="40"
                      height="40"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="#4a90e2"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <circle cx="12" cy="12" r="10"></circle>
                      <line x1="12" y1="8" x2="12" y2="16"></line>
                      <line x1="8" y1="12" x2="16" y2="12"></line>
                    </svg>
                  </button>
                </div>
              </div>
            </div>
          </form>
        </>
      }

      <span style={{ display: !status ? "block" : "none" }}>
        <strong style={{ color: "red" }}>
          * Keyword only contains alphabet , number, "_" character and not
          duplicate
        </strong>
      </span>
    </div>
  );
}
