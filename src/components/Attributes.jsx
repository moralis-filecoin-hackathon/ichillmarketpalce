import NewAttributes from "./NewAttributes";
import React, { useState, useEffect } from "react";
import { formatStrategyValues } from "rc-tree-select/lib/utils/strategyUtil";
export default function Attributes(props) {
  const [attData, setAttData] = useState(props.metadata);
  const [status, setStatus] = useState(true);
  const [_index, setIndex] = useState(0);

  useEffect(() => {
    // console.log(props.metadata)
    setAttData(props.metadata);
  }, [props.metadata]);

  const submitForm = (index) => {
    // props.metadata.attributes = [attr,...props.metadata.attributes]
    // const el = document.getElementById(`key` + index);
    // console.log("key", el.value)
    props.onDelData(index);
    // console.log("data",attr)
  };
  useEffect(() => {
    // console.log(props.metadata)
    setStatus(status);
    props.status(status);
  }, [status]);
  const validator = (index, event) => {
    event.preventDefault();
    setIndex(index);
    // get value input
    const re = /^[0-9a-zA-Z_]+$/;
    console.log(
      "testdata",
      event.target.value.includes(" ") || !re.test(event.target.value)
    );
    let data = false;
    if (event.target.value.includes(" ") || !re.test(event.target.value)) {
      setStatus(false);
    } else {
      setStatus(true);
      data = true;
    }
    //check space  value
    const isFound = props.metadata.attributes.some((element, inx) => {
      if (element.trait_type === event.target.value && index != inx) {
        return true;
      }

      return false;
    });
    // check duplicates  in key
    if (data && isFound == false) {
      props.metadata.attributes[index].trait_type = event.target.value;
    } else {
      setStatus(false);
    }
  };
  const [enterValue, setEnterValue] = useState("");
  const titlechangeValue = (key,event) => {
    
    // setEnterValue(event.target.value);
    if(key==="name"){
      props.metadata.name = event.target.value;
    }
    if(key==="description"){
      props.metadata.description = event.target.value;
    }
    
    setAttData(props.metadata);
    console.log("test", event.target.value);
  };

  const enterValueData = (index, event) => {
    event.preventDefault();
    setIndex(index);
    props.metadata.attributes[index].value = event.target.value;
    setAttData(props.metadata);
  }
  return (
    <div>

        <span style={{display: !status ? 'block' : 'none' }}><strong style={{color:"red"}}>* Keyword only contains alphabet , number, "_" character and not duplicate</strong></span>
        <div key="name" style={{ display: "flex" }}>
            <div style={{ display: "grid", width: "30%" }}>
              <input disabled
                className={`FormInput_input_popup`}
                defaultValue="name"
                placeholder="Enter a value"
              ></input>
            </div>
            <div style={{ display: "grid", width: "65%" }}>
              <input
                className="FormInput_input_popup"
                defaultValue={attData.name}
                placeholder="Enter a value"
                onChange={(e)=>{titlechangeValue("name",e)}}
              ></input>
            </div>
            <div style={{ display: "grid", width: "5%" }}>
              <div className="FormInput_delete_popup">
              </div>
            </div>
          </div>
          <div key="description" style={{ display: "flex" }}>
            <div style={{ display: "grid", width: "30%" }}>
              <input disabled
                className={`FormInput_input_popup`}
                defaultValue="description"
                placeholder="Enter a value"
              ></input>
            </div>
            <div style={{ display: "grid", width: "65%" }}>
              <input
                className="FormInput_input_popup"
                defaultValue={attData.description}
                onChange={(e)=>{titlechangeValue("description",e)}}
                placeholder="Enter a value"
              ></input>
            </div>
            <div style={{ display: "grid", width: "5%" }}>
              <div className="FormInput_delete_popup">
              </div>
            </div>
          </div>
      {attData.attributes.map((item, index) => (
        <>
          <div key={`${item.trait_type}-${index}`} style={{ display: "flex" }}>
            <div style={{ display: "grid", width: "30%" }}>
              <input
                className={`FormInput_input_popup ${
                  !status && _index === index ? "invalid" : ""
                }`}
                onChange={(e) => {
                  validator(index, e);
                }}
                defaultValue={item.trait_type}
                placeholder="Enter a value"
              ></input>
            </div>
            <div style={{ display: "grid", width: "65%" }}>
              <input
                className="FormInput_input_popup"
                defaultValue={item.value}
                placeholder="Enter a value"
                onChange={(e)=>{
                  enterValueData(index,e)
                }}
              ></input>
            </div>
            <div style={{ display: "grid", width: "5%" }}>
              <div className="FormInput_delete_popup">
                <svg
                  onClick={() => submitForm(index)}
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                >
                  {" "}
                  <g>
                    {" "}
                    <path fill="none" d="M0 0h24v24H0z" />{" "}
                    <path d="M4 8h16v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8zm2 2v10h12V10H6zm3 2h2v6H9v-6zm4 0h2v6h-2v-6zM7 5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v2h5v2H2V5h5zm2-1v1h6V4H9z" />{" "}
                  </g>{" "}
                </svg>
              </div>
            </div>
          </div>
        </>
      ))}
    </div>
  );
}
